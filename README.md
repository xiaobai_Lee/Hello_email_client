# Hello_email_client

#### 项目介绍
邮件客户端app

#### 软件架构
软件架构说明
采用MVP架构（可能是假的MVP）

软件包使用说明：  

1. bean:  
	Mail类： 一封邮件的详细信息  
	MailItem类： 一封邮件的简介，用于MailBoxActivity的UI显示  
	User类： 用户类，包括用户名，密码，SMTP及pop3服务器的相关信息（密码存到本地考虑一下加密）  
  
2. model:  
	ManageDao： 用于本地存储的相关交互管理，例如存储用户信息等  
	ManageDaoImpl： ManageDao的实现类，没有的话自己创建一个  
	TransmitMail： 用于和邮件服务器的交互，收发邮件  
	TransmitMailImpl： TransmitMail的实现类，没有的话自己创建一个  

3. myemail: (VIEW层？)  
	APP的活动类，控制APP的UI，我大概已经写好了，全部的数据动作我已经放到了对应的presenter包里面去了，  
	所以一般不用动这个包里的东西。这个包里面包含五个活动，分别对应APP里面的五个页面。  
	MainActivity： 主活动，APP的主页  
	MailBoxActivit： 添加进来的一个邮箱用户的邮箱页面，其中未读邮件被标红  
	AddAccountActivity： 添加用户页面  
	ShowMailActivity： 显示邮件详细信息页面  
	WriteMailActivity： 使用者书写邮件的页面  

4. presenter:  
	XXXXXXXXXXPresenter： APP活动类对应的presenter层的相关接口，进行数据和动作交互（例如保存填入的账号信息或者发送邮件等动作）  
	的一些方法都存储在对应的presenter类里面  
	XXXXXXXXXXPresenterImpl： 上面接口的实现类  

