package com.example.sea.presenter;

import android.content.Context;
import com.example.sea.bean.User;
import com.example.sea.model.ManageDaoImpl;

import java.util.ArrayList;

public class MainPresenterImpl implements MainPresenter {
    private Context mContext;
    private ManageDaoImpl dao;
    public MainPresenterImpl(Context context){
        mContext = context;
        dao = new ManageDaoImpl(mContext);
    }

    public ArrayList<String> getUsers() {
        ArrayList<String> username = new ArrayList<>();
        ArrayList<User> users=dao.getAllusers();
        for(User user:users) {
            String userName = user.getUserName();
            username.add(userName);
        }
        return username;
    }
}
