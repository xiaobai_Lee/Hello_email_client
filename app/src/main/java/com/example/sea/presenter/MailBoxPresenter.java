package com.example.sea.presenter;

import com.example.sea.bean.MailItem;

import java.util.ArrayList;

public interface MailBoxPresenter {
    /**
     * 从user得到对应的email的简述
     * @param user
     * @return
     */
    ArrayList<MailItem> getMailItems(String user);

    /**
     * 从本地注销用户名为user的用户，并且清除user的相关数据
     * @param user
     */
    void logout(String user);
}
