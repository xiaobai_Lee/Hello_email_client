package com.example.sea.presenter;

import android.content.Context;

import com.example.sea.bean.Mail;
import com.example.sea.model.ManageDao;
import com.example.sea.model.ManageDaoImpl;
import com.example.sea.model.TransmitMail;
import com.example.sea.bean.User;
import com.example.sea.model.TransmitMailImpl;

public class ShowMailPresenterImpl implements ShowMailPresenter {
    private Context mContext;
    private ManageDao md;
    private TransmitMail tsm;
    public ShowMailPresenterImpl(Context context){
        mContext = context;
        md=new ManageDaoImpl(mContext);
        tsm =new TransmitMailImpl(mContext);
    }
    public Mail getMail(String mailId) {
        String [] arr = mailId.split("\\s+");
        User user=md.getUser(arr[0]);
        Mail mail=tsm.getMail(user, Integer.valueOf(arr[1]));
        return mail;
    }

    @Override
    public Mail getDraft() {
        Mail mail = md.getDraft();
        return mail;
    }
}
