package com.example.sea.presenter;

import android.content.Context;

import com.example.sea.bean.Mail;
import com.example.sea.model.ManageDao;
import com.example.sea.model.ManageDaoImpl;
import com.example.sea.model.TransmitMail;
import com.example.sea.model.TransmitMailImpl;

import javax.mail.MessagingException;


public class WriteMailPresenterImpl implements WriteMailPresenter {
    private Context mContext;
    private ManageDao md;
    private TransmitMail tsm;
    public WriteMailPresenterImpl(Context context){
        mContext = context;
        md=new ManageDaoImpl(mContext);
        tsm=new TransmitMailImpl(mContext);
    }

    @Override
    public Mail getDraft() {
        Mail mail=md.getDraft();
        return mail;
    }

    @Override
    public void saveMail(Mail mail) {      //可以调用protocol里面的邮件发送
        // bala bala bala
        md.saveDraft(mail);

    }

    @Override
    public int sendMail(Mail mail) {
        // bala bala bala
        int w;
        w=tsm.sendMail(mail);
        return w;
    }
    @Override
    public boolean jiance(String email) {
        //判断是否为空邮箱
        int k = 0;
        if(email == null) {
            return false;
        }
         /*
          * 单引号引的数据 是char类型的
                                    双引号引的数据 是String类型的
                                    单引号只能引一个字符
                                    而双引号可以引0个及其以上*
          */

        //判断是否有仅有一个@且不能在开头或结尾
        if(email.indexOf("@") > 0 && email.indexOf('@') == email.lastIndexOf('@') && email.indexOf('@') < email.length()-1) {
            k++;
        }

        //判断"@"之后必须有"."且不能紧跟
        if(email.indexOf('.',email.indexOf('@')) > email.indexOf('@')+1 ) {
            k++;
        }
        //判断"@"之前或之后不能紧跟"."
        if(email.indexOf('.') < email.indexOf('@')-1 || email.indexOf('.') > email.indexOf('@')+1 ) {
            k++;
        }
        //@之前要有6个字符
        if(email.indexOf('@') > 5 ) {
            k++;
        }

        if(email.endsWith("com") || email.endsWith("org") || email.endsWith("cn") ||email.endsWith("net")) {
            k++;
        }
        if(k == 5) {
            return true;
        }
        return false;

    }

    @Override
    public boolean checkFormate(Mail mail) { //审查邮件格式
        //bala bala bala
        if (mail.getSender()==null || jiance(mail.getSender())==false) return false;
        if (mail.getReceiver()==null || jiance(mail.getReceiver())==false) return false;
        if (mail.getContent().equals("") || mail.getContent()==null) return false;
        if (mail.getSubject().equals("") || mail.getSubject()==null) return false;
        return true;
    }

    @Override
    public void saveOK(Mail mail) {
        //bala bala bala
        md.saveSendMail(mail);
    }
}
