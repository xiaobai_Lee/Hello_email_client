package com.example.sea.presenter;

import com.example.sea.bean.Mail;

public interface ShowMailPresenter {
    /**
     * 得到mailId对应的邮件内容，mailId可以是： 用户名+空格+第几封邮件 的字符串
     * @param mailId
     * @return
     */
    Mail getMail(String mailId);

    /**
     * 得到草稿内容
     * @return
     */
    Mail getDraft();
}
