package com.example.sea.presenter;

import com.example.sea.bean.User;

public interface AddAccountPresenter {
    /**
     * 将用户保存到本地
     * @param user
     * @return
     */
    boolean saveAccount(User user);

    /**
     * 检查输入的用户是否合法,1为合法，0为用户名不合法, -1为SMTP错误，-2为pop3错误
     * @param user
     * @return
     */
    int checkAccount(User user);
}
