package com.example.sea.presenter;

import android.content.Context;

import com.example.sea.bean.MailItem;
import com.example.sea.model.ManageDaoImpl;
import com.example.sea.model.TransmitMailImpl;
import com.example.sea.bean.User;

import java.util.ArrayList;

public class MailBoxPresenterImpl implements MailBoxPresenter {

    private Context mContext;
    private TransmitMailImpl tdao;
    private ManageDaoImpl dao;
    public MailBoxPresenterImpl(Context context){
        mContext = context;
        tdao=new TransmitMailImpl(mContext);
        dao = new ManageDaoImpl(mContext);
    }

    public ArrayList<MailItem> getMailItems(String user) {     //根据user得到邮件的简述信息，需要调用pop3从服务器拉取信息，并且和本地的信息比较
        User users=dao.getUser(user);
        ArrayList<MailItem>  items=tdao.getAllMail(users);
        return items;
    }

    @Override
    public void logout(String user) {
        // logout bala bala
         dao.deleteUser(user);
    }
}
