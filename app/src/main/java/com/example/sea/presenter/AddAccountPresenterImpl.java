package com.example.sea.presenter;

import android.content.Context;

import com.example.sea.bean.Mail;
import com.example.sea.bean.MailItem;
import com.example.sea.bean.User;
import com.example.sea.LocalDatabase.LocalDatabaseHelper;
import com.example.sea.model.ManageDaoImpl;
import com.example.sea.model.TransmitMail;
import com.example.sea.model.TransmitMailImpl;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddAccountPresenterImpl implements AddAccountPresenter {
   // @Override
    private Context mContext;
    private ManageDaoImpl dao;
    private TransmitMail tmail;

    public AddAccountPresenterImpl(Context context){
        mContext = context;
        dao = new ManageDaoImpl(mContext);
        tmail = new TransmitMailImpl(mContext);
    }
    public boolean saveAccount(User user) {
        if(dao.saveUser(user)==0){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public int checkAccount(User user) {
        Pattern p = Pattern.compile("\\w+@(\\w+.)+[a-z]{2,3}");
        Matcher m = p.matcher(user.getUserName());
        boolean b = true;//m.matches();
        if(b){
            if (tmail.isOKofSMTP(user)==false) return -1;
            if (tmail.isOKofPOP3(user)==false) return -2;
            return 1;
        }
        return 0;
    }
}
