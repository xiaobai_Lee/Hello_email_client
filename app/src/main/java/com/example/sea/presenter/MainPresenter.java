package com.example.sea.presenter;

import java.util.ArrayList;
import java.util.List;

public interface MainPresenter {
    /**
     * 得到保存到本地的所有用户名
     * @return
     */
    ArrayList<String> getUsers();
}
