package com.example.sea.presenter;

import com.example.sea.bean.Mail;

import javax.mail.MessagingException;

public interface WriteMailPresenter {
    /**
     * 得到草稿箱里面存到的那一封邮件
     * @return
     */
    Mail getDraft();

    /**
     * 保存邮件到草稿箱,并且把原来的草稿更新（删除？）
     * @param mail
     */
    void saveMail(Mail mail);

    /**
     * 用于把mail邮件发送出去, 0代表成功，错误返回发送错误的错误代码
     * @param mail
     */
    int sendMail(Mail mail) throws MessagingException;

    /**
     * 邮件格式审查，例如有没有空字段等
     * @param mail
     * @return
     */
    boolean checkFormate(Mail mail);

    /**
     * 保存已经发送的邮件，暂时只存一封，把之前的那份给删掉
     * @param mail
     */
    void saveOK(Mail mail);
    boolean jiance(String email);
}
