package com.example.sea.bean;

public class MailItem {
    private int id;
    private String subject;
    private String time;
    private String contentResume;
    private boolean isRead;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public MailItem(String subject, String time, String contentResume, boolean isRead) {
        this.subject = subject;
        this.time = time;
        this.contentResume = contentResume;
        this.isRead = isRead;
    }
    public MailItem() {};

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContentResume() {
        return contentResume;
    }

    public void setContentResume(String contentResume) {
        this.contentResume = contentResume;
    }
}
