package com.example.sea.bean;

public class User {
    private String userName;       //haha@test.com
    private String password;
    private String smtp;
    private int smtpPort;
    private String pop3;
    private int pop3Port;

    public User(String name, String password, String smtp, String pop3) {
        this.userName = name;
        this.password = password;
        this.smtp = smtp;
        this.pop3 = pop3;
    }

    public User() {};

    public String getSmtp() {
        return smtp;
    }

    public String getPop3() {
        return pop3;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setUserName(String name) {
        this.userName = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public void setPop3(String pop3) {
        this.pop3 = pop3;
    }

    public int getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(int smtpPort) {
        this.smtpPort = smtpPort;
    }

    public int getPop3Port() {
        return pop3Port;
    }

    public void setPop3Port(int pop3Port) {
        this.pop3Port = pop3Port;
    }
}
