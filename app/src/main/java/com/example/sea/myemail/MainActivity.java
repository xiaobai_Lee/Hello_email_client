package com.example.sea.myemail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sea.bean.User;
import com.example.sea.presenter.MainPresenter;
import com.example.sea.presenter.MainPresenterImpl;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    MainPresenterImpl mainPresenter = new MainPresenterImpl(this);

    private void initLayout(ArrayList<String> users) {
        LinearLayout acounts = findViewById(R.id.acounts);
        for (int i=0; i<users.size(); ++i) {
            final String user = users.get(i);
            View view = LayoutInflater.from(this).inflate(R.layout.acount_item, acounts, false);
            ((ImageView) view.findViewById(R.id.acount_image)).setImageResource(R.mipmap.mail);
            view.setOnClickListener(new View.OnClickListener() {     //加入下一步的活动
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent("com.example.sea.MAILBOX");
                    intent.putExtra("user", user);      //传入邮箱用户名
                    startActivity(intent);
                }
            });
            ((TextView) view.findViewById(R.id.acount_name)).setText(user);
            acounts.addView(view);
        }
        View view = LayoutInflater.from(this).inflate(R.layout.acount_item, acounts, false);
        view.setClickable(true);
        view.setBackgroundResource(R.drawable.selected_bg);
        ((ImageView) view.findViewById(R.id.acount_image)).setImageResource(R.mipmap.add);
        view.setOnClickListener(new View.OnClickListener() {     //加入下一步的活动
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.sea.ADDACCOUNT"));       //在这通过intent加入新活动
            }
        });
        ((TextView) view.findViewById(R.id.acount_name)).setText("添加用户");
        acounts.addView(view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<String> users = mainPresenter.getUsers();
        for (int i=0; i<users.size(); ++i) {
            String user = users.get(i);
            System.out.print(user);
        }
        initLayout(users);
        ((LinearLayout)findViewById(R.id.cg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.sea.WRITEMAIL"));
            }
        });
        ((LinearLayout)findViewById(R.id.yfs)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.sea.SHOWMAIL"));
            }
        });
    }
}
