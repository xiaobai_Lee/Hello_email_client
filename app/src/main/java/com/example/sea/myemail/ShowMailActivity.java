package com.example.sea.myemail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.sea.bean.Mail;
import com.example.sea.presenter.ShowMailPresenterImpl;

public class ShowMailActivity extends AppCompatActivity {
    private ShowMailPresenterImpl showMail = new ShowMailPresenterImpl(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_mail_layout);
        Intent intent = getIntent();
        String mailId = intent.getStringExtra("mailId");
//         得到mailId对应的邮件内容， balabala
        Mail mail;
        if (mailId==null || mailId.equals("")) {
            mail = showMail.getDraft();
        }
        else {
            mail = showMail.getMail(mailId);
        }
        ((TextView)findViewById(R.id.show_subject)).setText(mail.getSubject());
        ((TextView)findViewById(R.id.show_receiver)).setText(mail.getReceiver());
        ((TextView)findViewById(R.id.show_sender)).setText(mail.getSender());
        ((TextView)findViewById(R.id.show_time)).setText(mail.getTime());
        ((TextView)findViewById(R.id.show_content)).setText(mail.getContent());
    }
}
