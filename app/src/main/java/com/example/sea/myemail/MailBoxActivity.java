package com.example.sea.myemail;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sea.bean.MailItem;
import com.example.sea.presenter.MailBoxPresenter;
import com.example.sea.presenter.MailBoxPresenterImpl;

import java.util.ArrayList;

public class MailBoxActivity extends AppCompatActivity {

    private MailBoxPresenterImpl mailBox = new MailBoxPresenterImpl(this);        //MailBoxActivity的控制类
    private String user;

    private void initLayout(ArrayList<MailItem> items) {
        LinearLayout mails = findViewById(R.id.box_list);
        for (int i=0; i<items.size(); ++i) {
            final MailItem item = items.get(i);
            View view = LayoutInflater.from(this).inflate(R.layout.mail_item, mails, false);
            TextView subject = (TextView)view.findViewById(R.id.item_subject);
            subject.setText(item.getSubject());
            if (item.isRead() == false) subject.setTextColor(getResources().getColor(R.color.red));        //未读标红
            ((TextView)view.findViewById(R.id.item_time)).setText(item.getTime());
            ((TextView)view.findViewById(R.id.item_contents)).setText(item.getContentResume());
            final int id = item.getId();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent("com.example.sea.SHOWMAIL");
                    intent.putExtra("mailId", id);                                    //传入id给详细显示页面
                    startActivity(intent);
                }
            });
            mails.addView(view);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.mail_box_layout);
        Intent intent = getIntent();
        user = intent.getStringExtra("user");    //得到上个传入的用户名
        ArrayList<MailItem> items = mailBox.getMailItems(user);
        initLayout(items);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.write_mail:
                Intent intent = new Intent("com.example.sea.WRITEMAIL");
                intent.putExtra("user", user);
                startActivity(intent);
                break;
            case R.id.logout:
                Toast.makeText(this, "logout", Toast.LENGTH_SHORT).show();
                mailBox.logout(user);
                break;
            default:
        }
        return true;
    }
}
