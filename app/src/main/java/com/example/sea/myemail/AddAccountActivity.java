package com.example.sea.myemail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sea.bean.User;
import com.example.sea.presenter.AddAccountPresenter;
import com.example.sea.presenter.AddAccountPresenterImpl;

public class AddAccountActivity extends AppCompatActivity {
    AddAccountPresenterImpl addAccount = new AddAccountPresenterImpl(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_acount_layout);
        ((Button)findViewById(R.id.add_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User();
                String s = ((EditText)findViewById(R.id.username)).getText().toString();
                user.setUserName(s);
                s = ((EditText)findViewById(R.id.password)).getText().toString();
                user.setPassword(s);
                s = ((EditText)findViewById(R.id.smtp_server)).getText().toString();
                user.setSmtp(s);
                s = ((EditText)findViewById(R.id.smtp_port)).getText().toString();
                if (s.equals("")) s="0";
                user.setSmtpPort(Integer.valueOf(s));
                s = ((EditText)findViewById(R.id.pop_server)).getText().toString();
                user.setPop3(s);
                s = ((EditText)findViewById(R.id.pop_port)).getText().toString();
                if (s.equals("")) s="0";
                user.setPop3Port(Integer.valueOf(s));
                int returnCode = addAccount.checkAccount(user);
                if (returnCode != 0) {
                    addAccount.saveAccount(user);
                    finish();
                }
                else
                {
                    Toast.makeText(AddAccountActivity.this, "填入格式有问题", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
