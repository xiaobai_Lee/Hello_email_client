package com.example.sea.myemail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sea.bean.Mail;
import com.example.sea.presenter.WriteMailPresenter;
import com.example.sea.presenter.WriteMailPresenterImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class WriteMailActivity extends AppCompatActivity {
    WriteMailPresenterImpl writeMail = new WriteMailPresenterImpl(this);

    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_mail_layout);
        Intent intent = getIntent();
        user = intent.getStringExtra("user");
//        final ProgressDialog progressDialog = new ProgressDialog(WriteMailActivity.this);
//        progressDialog.setTitle("发送中");
//        progressDialog.setMessage("Waiting...");
//        progressDialog.setCancelable(true);
        if (user==null) {                           //邮件从草稿箱而来，读取草稿箱里面的那封邮件
            Mail mail = writeMail.getDraft();
            ((EditText)findViewById(R.id.sender)).setText(mail.getSender());
            ((EditText)findViewById(R.id.receiver)).setText(mail.getReceiver());
            ((EditText)findViewById(R.id.write_subject)).setText(mail.getSubject());
            ((EditText)findViewById(R.id.write_contents)).setText(mail.getContent());
        }
        else {                                     //重新写的邮件，读取到了发送者user
            ((EditText)findViewById(R.id.sender)).setText(user);
        }
        ((Button)findViewById(R.id.save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mail mail = new Mail();
                String s = ((EditText)findViewById(R.id.sender)).getText().toString();
                mail.setSender(s);
                s = ((EditText)findViewById(R.id.receiver)).getText().toString();
                mail.setReceiver(s);
                s = ((EditText)findViewById(R.id.write_subject)).getText().toString();
                mail.setSubject(s);
                s = ((EditText)findViewById(R.id.write_contents)).getText().toString();
                mail.setContent(s);
                if (writeMail.checkFormate(mail) == true) {
                    writeMail.saveMail(mail);
                    Toast.makeText(WriteMailActivity.this, "邮件保存成功", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(WriteMailActivity.this, "邮件格式有误", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((Button)findViewById(R.id.send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mail mail = new Mail();
                String s = ((EditText)findViewById(R.id.sender)).getText().toString();
                mail.setSender(s);
                s = ((EditText)findViewById(R.id.receiver)).getText().toString();
                mail.setReceiver(s);
                s = ((EditText)findViewById(R.id.write_subject)).getText().toString();
                mail.setSubject(s);
                s = ((EditText)findViewById(R.id.write_contents)).getText().toString();
                mail.setContent(s);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                mail.setTime(df.format(System.currentTimeMillis()));
                if (writeMail.checkFormate(mail) == true) {
//                    progressDialog.show();
                    int returnCode = writeMail.sendMail(mail);
//                    progressDialog.dismiss();
                    if (returnCode == 0) {
                        Toast.makeText(WriteMailActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                        writeMail.saveOK(mail);
                        finish();
                    }
                    else {
                        Toast.makeText(WriteMailActivity.this, "发送失败，错误代码为 "+returnCode, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(WriteMailActivity.this, "邮件格式有误，请重新检查", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
