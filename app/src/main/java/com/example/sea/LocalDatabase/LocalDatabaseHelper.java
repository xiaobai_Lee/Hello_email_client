package com.example.sea.LocalDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class LocalDatabaseHelper extends SQLiteOpenHelper {
    public static final String CREATE_USER = "create table User("
            +"userName text primary key,"
            +"password text,"
            +"smtp text,"
            +"smtpPort integer,"
            +"pop3 text,"
            +"pop3Port integer)";

    public static final String CREATE_MAIL = "create table Mail("
            +"id text primary key,"
            +"receiver text,"
            +"sender text,"
            +"time text,"
            +"subject text,"
            +"content text)";

    public static final String CREATE_MAILITEM = "create table MailItem("
            +"id text primary key,"
            +"subject text,"
            +"time text,"
            +"contentResume text,"
            +"isRead boolean)";

    public static final String CREATE_DRAFT = "create table Draft("
            +"id text primary key,"
            +"receiver text,"
            +"sender text,"
            +"time text,"
            +"subject text,"
            +"content text)";

    public static final String CREATE_SENDMAIL= "create table SendMail("
            +"id text primary key,"
            +"receiver text,"
            +"sender text,"
            +"time text,"
            +"subject text,"
            +"content text)";

    private Context mContext;

    public LocalDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context,name,factory,version);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_USER);
        db.execSQL(CREATE_MAIL);
        db.execSQL(CREATE_MAILITEM);
        db.execSQL(CREATE_DRAFT);
        db.execSQL(CREATE_SENDMAIL);
        Toast.makeText(mContext,"create succeeded",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        db.execSQL("drop table if exists User");
        db.execSQL("drop table if exists Mail");
        db.execSQL("drop table if exists MailItem");
        db.execSQL("drop table if exists Draft");
        db.execSQL("drop table if exists SendMail");
        onCreate(db);
    }
}
