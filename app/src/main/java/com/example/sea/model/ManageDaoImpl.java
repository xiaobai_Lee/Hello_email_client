package com.example.sea.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.sea.LocalDatabase.LocalDatabaseHelper;
import com.example.sea.bean.Mail;
import com.example.sea.bean.User;

import java.io.IOException;
import java.util.ArrayList;

public class ManageDaoImpl implements ManageDao {
    private Context mContext;
    private LocalDatabaseHelper dbHelper;

    public ManageDaoImpl(Context context){
        mContext = context;
        dbHelper = new LocalDatabaseHelper(mContext,"MailClient.db",null,1);
    }
    /**
     * 将user信息存到本地，密码存到本地加密一下？
     * @param user
     * @return
     */
    @Override
    public int saveUser(User user){
        String userName = user.getUserName();
        String password = user.getPassword();
        String smtp = user.getSmtp();
        int smtpPort = user.getSmtpPort();
        String pop3 = user.getPop3();
        int pop3Port = user.getPop3Port();

        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.execSQL("insert into User(userName,password,smtp,smtpPort,pop3,pop3Port) values(?,?,?,?,?,?)",
                    new String[]{userName,password,smtp,String.valueOf(smtpPort),pop3,String.valueOf(pop3Port)});
            db.close();
            return 0;
        }catch(Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 得到本地用户名为userName的用户信息 userName为类似haha@test.com的字符串，所以能保持数据唯一性
     * @param userName
     * @return
     */
    @Override
    public User getUser(String userName){
        User user = new User();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor= db.rawQuery("select * from User where userName=?",new String[]{userName});
        if(cursor.moveToFirst()){
            do{
                user.setUserName(userName);
                user.setPassword(cursor.getString(cursor.getColumnIndex("password")));
                user.setSmtp(cursor.getString(cursor.getColumnIndex("smtp")));
                user.setSmtpPort(cursor.getInt(cursor.getColumnIndex("smtpPort")));
                user.setPop3(cursor.getString(cursor.getColumnIndex("pop3")));
                user.setPop3Port(cursor.getInt(cursor.getColumnIndex("pop3Port")));
            }while(cursor.moveToNext());
        }else{
            Toast.makeText(mContext,"没有用户数据",Toast.LENGTH_SHORT).show();
        }
        cursor.close();
        db.close();
        return user;
    }
    /**
     * 获取所有本地用户
     * @return
     */
    @Override
    public ArrayList<User> getAllusers(){
        ArrayList<User> users = new ArrayList<User>();
        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor cursor= db.rawQuery("select * from User",null);
            if(cursor.moveToFirst()){
                do{
                    User user = new User();
                    user.setUserName(cursor.getString(cursor.getColumnIndex("userName")));
                    user.setPassword(cursor.getString(cursor.getColumnIndex("password")));
                    user.setSmtp(cursor.getString(cursor.getColumnIndex("smtp")));
                    user.setSmtpPort(cursor.getInt(cursor.getColumnIndex("smtpPort")));
                    user.setPop3(cursor.getString(cursor.getColumnIndex("pop3")));
                    user.setPop3Port(cursor.getInt(cursor.getColumnIndex("pop3Port")));
                    users.add(user);
                }while(cursor.moveToNext());
                cursor.close();
                db.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return users;
    }

    /**
     * 注销用户时，从本地数据库删除该用户,userName作为唯一标识符
     * @param userName
     * @return
     */
    @Override
    public int deleteUser(String userName){
        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.execSQL("delete from User where userName=?",new String[]{userName});
            db.close();
        }catch(Exception e){
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    /**
     * 将mail保存到草稿箱，保存的时候，把上一封给删掉（目前只能存一封）
     * @param mail
     * @return
     */
    @Override
    public int saveDraft(Mail mail){
        String id = mail.getId();
        String receiver = mail.getReceiver();
        String sender = mail.getSender();
        String time = mail.getTime();
        String subject = mail.getSubject();
        String content = mail.getContent();

        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.execSQL("delete from Draft where 1=1");
            db.execSQL("insert into Draft(id,receiver,sender,time,subject,content) values(?,?,?,?,?,?)",
                    new String[]{id,receiver,sender,time,subject,content});
            db.close();
            return 0;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 得到草稿箱里面的内容
     * @return
     */
    @Override
    public Mail getDraft(){
        Mail mail = new Mail();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor= db.rawQuery("select * from Draft",null);
        if(cursor.moveToFirst()){
            do{
                mail.setId(cursor.getString(cursor.getColumnIndex("id")));
                mail.setReceiver(cursor.getString(cursor.getColumnIndex("receiver")));
                mail.setSender(cursor.getString(cursor.getColumnIndex("sender")));
                mail.setTime(cursor.getString(cursor.getColumnIndex("time")));
                mail.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
                mail.setContent(cursor.getString(cursor.getColumnIndex("content")));
            }while(cursor.moveToNext());
        }else{
             mail.setId("0");
             mail.setReceiver("");
             mail.setSender("");
             mail.setTime("");
             mail.setSubject("");
             mail.setContent("");
        }
        db.close();
        return mail;
    }

    /**
     * 将mail保存到已发送邮件，保存的时候，把上一封给删掉（目前只能存一封）
     * @param mail
     * @return
     */
    @Override
    public int saveSendMail(Mail mail){
        String id = mail.getId();
        String receiver = mail.getReceiver();
        String sender = mail.getSender();
        String time = mail.getTime();
        String subject = mail.getSubject();
        String content = mail.getContent();

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("delete from SendMail where 1=1");
        db.execSQL("insert into SendMail(id,receiver,sender,time,subject,content) values(?,?,?,?,?,?)",
                new String[]{id,receiver,sender,time,subject,content});
        db.close();
        return 0;
    }

    /**
     * 得到保存到本地的已发送邮件
     * @return
     */
    @Override
    public Mail getSendMail(){
        Mail mail = new Mail();

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor= db.rawQuery("select * from SendMail",null);
        if(cursor.moveToFirst()){
            do{
                mail.setId(cursor.getString(cursor.getColumnIndex("id")));
                mail.setReceiver(cursor.getString(cursor.getColumnIndex("receiver")));
                mail.setSender(cursor.getString(cursor.getColumnIndex("sender")));
                mail.setTime(cursor.getString(cursor.getColumnIndex("time")));
                mail.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
                mail.setContent(cursor.getString(cursor.getColumnIndex("content")));
            }while(cursor.moveToNext());
        }else{
            mail.setId("0");
            mail.setReceiver("");
            mail.setSender("");
            mail.setTime("");
            mail.setSubject("");
            mail.setContent("");
        }
        db.close();
        return mail;
    }
}
