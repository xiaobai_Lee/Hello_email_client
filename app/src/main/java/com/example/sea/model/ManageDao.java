package com.example.sea.model;

import com.example.sea.bean.Mail;
import com.example.sea.bean.User;

import java.util.ArrayList;

public interface ManageDao {
    /**
     * 将user信息存到本地，密码存到本地加密一下？
     * @param user
     * @return
     */
    int saveUser(User user);

    /**
     * 得到本地用户名为userName的用户信息 userName为类似haha@test.com的字符串，所以能保持数据唯一性
     * @param userName
     * @return
     */
    User getUser(String userName);

    /**
     * 获取所有本地用户
     * @return
     */
    ArrayList<User> getAllusers();
    /**
     * 注销用户时，从本地数据库删除该用户
     * @param userName
     * @return
     */
    int deleteUser(String userName);

    /**
     * 将mail保存到草稿箱，保存的时候，把上一封给删掉（目前只能存一封）
     * @param mail
     * @return
     */
    int saveDraft(Mail mail);

    /**
     * 得到草稿箱里面的内容
     * @return
     */
    Mail getDraft();

    /**
     * 将mail保存到已发送邮件，保存的时候，把上一封给删掉（目前只能存一封）
     * @param mail
     * @return
     */
    int saveSendMail(Mail mail);

    /**
     * 得到保存到本地的已发送邮件
     * @return
     */
    Mail getSendMail();
}
