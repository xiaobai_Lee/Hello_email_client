package com.example.sea.model;

import com.example.sea.bean.Mail;
import com.example.sea.bean.MailItem;
import com.example.sea.bean.User;

import java.util.ArrayList;

public interface TransmitMail {      //可以使用javaMail实现
    /**
     * 得到用户user的第id封邮件
     * @param user
     * @param id
     * @return
     */
    Mail getMail(User user, int id);

    /**
     * 得到user的所有邮件简述
     * @param user
     * @return
     */
    ArrayList<MailItem> getAllMail(User user);

    /**
     * 将mail发送出去，发送成功返回0，错误返回错误代码
     * @param mail
     * @return
     */
    int sendMail(Mail mail);

    /**
     * 判断user是否能够连接上smtp
     * @param user
     * @return
     */
    boolean isOKofSMTP(User user);

    /**
     * 判断user是否能够连接上pop3
     * @param user
     * @return
     */
    boolean isOKofPOP3(User user);
}
